﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PhotoGallery.Models
{
    public class Comment
    {
        public int CommentId { get; set; }

        [Required(ErrorMessage = "محتوى التعليق مطلوب")]
        public string Text { get; set; }

        public DateTime Date { get; set; }

        [Required]
        [StringLength(80, ErrorMessage = " اسم المعلق لا يمكن ان يتجاوز 80 حرفا")]
        [System.ComponentModel.DataAnnotations.Display(Name ="Author")]
        public string Author { get; set; }

        public virtual Picture Picture { get; set; }

        public virtual int PictureId{get;set;}
    }
}