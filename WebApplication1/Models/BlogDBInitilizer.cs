﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhotoGallery.Models
{
    public class BlogDBInitilizer :System.Data.Entity.DropCreateDatabaseIfModelChanges<PhotoGalleryContext>
    {
        protected override void Seed(PhotoGalleryContext context)
        {
            var Commentss = new List<Comment>
            {
                new Comment{ Author="Muhammad Albalawi", Date = DateTime.Now, Text="Worst Post i every read in my life"},
                new Comment{ Author="Muhammad Albalawi", Date = DateTime.Now, Text="Hello there , htis is the central scuritizer"},
                new Comment{ Author="Muhammad Albalawi", Date = DateTime.Now, Text="a Little green rosetta"},
                new Comment{ Author="Muhammad Albalawi", Date = DateTime.Now, Text="Good Pictures"},
                new Comment{ Author="Muhammad Albalawi", Date = DateTime.Now, Text="Worst Post i every read in my life"}
            };
            new List<Picture>
            {
                new Picture() { Title="Steps", Author = "Malbalawi", URL="/Content/StockImages/file0001079221497.jpg", Caption = "Hello world", Date = DateTime.Now, Rating = rating.Good , Comments = Commentss },
                new Picture() { Title = "Droplets", Author = "Malbalawi", Caption = "Hello world", URL="/Content/StockImages/file0001116000079.jpg", Date = DateTime.Now, Rating = rating.Good },
                new Picture() { Title = "Lady Wearing Black", Author = "Malbalawi", Caption = "Black and White", URL="/Content/StockImages/file0001209214386.jpg", Date = DateTime.Now, Rating = rating.meh},
                new Picture() { Title = "Autum", Author = "Muhammad Albalawi", Caption = "Autum", URL="/Content/StockImages/file0001625591306.jpg", Date = DateTime.Now, Rating = rating.Good },
                new Picture() { Title = "Chess Board", Author = "Muhammad Albalawi", Caption = "Chess Board", URL="/Content/StockImages/file0001706961259.jpg", Date = DateTime.Now, Rating = rating.Good },
                new Picture() { Title = "water drop", Author = "Muhammad Albalawi", Caption = "Water drop", URL="/Content/StockImages/file0001141038889.jpg", Date = DateTime.Now, Rating = rating.Good },
                new Picture() { Title = "Coastal city", Author = "Muhammad Albalawi", Caption = "City", URL="/Content/StockImages/file0001176452626.jpg", Date = DateTime.Now, Rating = rating.Good },                
                new Picture() { Title = "Red Head Girl", Author = "Muhammad Albalawi", Caption = "Red Head Girl", URL="/Content/StockImages/file0001224117612.jpg", Date = DateTime.Now, Rating = rating.Good },
                new Picture() { Title = "Watch", Author = "Muhammad Albalawi", Caption = "Hand Watch", URL="/Content/StockImages/file0001316404158.jpg", Date = DateTime.Now, Rating = rating.Good },
                new Picture() { Title = "Taj Mahal  ", Author = "Muhammad Albalawi", Caption = "Taj Mahal", URL="/Content/StockImages/file000132701536.jpg", Date = DateTime.Now, Rating = rating.Good },
                new Picture() { Title = "Sea Shore", Author = "Muhammad Albalawi", Caption = "Sea Shore", URL="/Content/StockImages/file0001376718168.jpg", Date = DateTime.Now, Rating = rating.Good },
                new Picture() { Title = "Clouds", Author = "Muhammad Albalawi", Caption = "Cloads in blue sky", URL="/Content/StockImages/file0001454659375.jpg", Date = DateTime.Now, Rating = rating.Good },
                new Picture() { Title = "Musical Notes", Author = "Muhammad Albalawi", Caption = "Musical Notes", URL="/Content/StockImages/file0001545806234.jpg", Date = DateTime.Now, Rating = rating.Good },
                new Picture() { Title = "Golf Club", Author = "Muhammad Albalawi", Caption = "Golf Club", URL="/Content/StockImages/file0001565782100.jpg", Date = DateTime.Now, Rating = rating.Good },
                new Picture() { Title = "Grass", Author = "Muhammad Albalawi", Caption = "Grass", URL="/Content/StockImages/file0001601969844.jpg", Date = DateTime.Now, Rating = rating.Good },
                new Picture() { Title = "Typewriter", Author = "Muhammad Albalawi", Caption = "typewriter", URL="/Content/StockImages/file0001608482449.jpg", Date = DateTime.Now, Rating = rating.Good },
                new Picture() { Title = "Wheat", Author = "Muhammad Ahmad Albalawi", Caption = "Wheat", URL="/Content/StockImages/file0001637922945.jpg", Date = DateTime.Now, Rating = rating.Good }
               
            }.ForEach(m => context.Pictures.Add(m));
        

            base.Seed(context); 
        }
    }
}