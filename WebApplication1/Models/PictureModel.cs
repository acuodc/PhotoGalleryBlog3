﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhotoGallery.Models
{
    public enum rating { Good, Bad , meh};

    public class Picture
    {
        
        public int PictureId { get; set; }
        public string Title { get; set; }
        public string Caption { get; set; }
        public string URL { get; set; }
        public DateTime Date { get; set; }
        public string  Author { get; set; }
        public  rating Rating { get; set; }
        public virtual List<Comment> Comments { get; set; }
    }

   


}