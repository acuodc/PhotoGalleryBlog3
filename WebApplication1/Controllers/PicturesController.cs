﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PhotoGallery.Models;
using Microsoft.AspNet.Identity;

namespace PhotoGallery.Controllers
{
    public class PicturesController : Controller
    {
        private PhotoGalleryContext db = new PhotoGalleryContext();

        // GET: Pictures
        public ActionResult Index()
        {
            
           Random m = new Random();
            return View(db.Pictures.OrderBy( i =>  Guid.NewGuid() ).ToList());
        }
        public ActionResult Search( string q)
        {
            ViewBag.searchTerm = q;
            return View("Index",db.Pictures.Where(m => m.Title.Contains(q)).ToList());
        }
        // GET: Pictures/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Picture picture = db.Pictures.Find(id);
            if (picture == null)
            {
                return HttpNotFound();
            }
            return View(picture);
        }

        
        // GET: Pictures/Create
        [Authorize]
        public ActionResult Create()
        {
                return View();
        }

        // POST: Pictures/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PictureId,Title,Caption,Date,Author,Rating")] Picture picture, HttpPostedFileBase file)
        {
            if (file.ContentLength == 0)
            {
                ModelState.AddModelError("invalidFile", "Empty file");
            }
            else if (!file.FileName.EndsWith(".jpg"))
            {
                ModelState.AddModelError("invalid format", "my website only accept .jpg format");              
            }
            if (ModelState.IsValid)
            {
                string path = System.IO.Path.Combine( Server.MapPath("~/Content/StockImages"), file.FileName);
                file.SaveAs(path);
                picture.URL = "/Content/StockImages/"+ file.FileName;

                db.Pictures.Add(picture);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(picture);
        }

        // GET: Pictures/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Picture picture = db.Pictures.Find(id);
            if (picture == null)
            {
                return HttpNotFound();
            }
            return View(picture);
        }

        // POST: Pictures/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PictureId,Title,Caption,URL,Date,Author,Rating")] Picture picture)
        {
            if (ModelState.IsValid)
            {
                db.Entry(picture).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(picture);
        }
      
        // GET: Pictures/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Picture picture = db.Pictures.Find(id);
            if (picture == null)
            {
                return HttpNotFound();
            }
            return View(picture);
        }

        // POST: Pictures/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Picture picture = db.Pictures.Find(id);
            db.Pictures.Remove(picture);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
